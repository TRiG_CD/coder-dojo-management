<?php
require 'functions.php';

if (!$parent = (int)get('id')) {
    return go('index.php');
}

$d = new Data;
$p = $d->row('Parent', $parent, array('FirstName', 'LastName'));

$date  = post('Year');
$date .= '-';
$date .= str_pad(post('Month'), 2, '0', STR_PAD_LEFT);
$date .= '-01'; // for data protection, not storing actual date

$a = array(
    'parent_id'  => $parent,
    'Name'       => post('Name'),
    'BirthMonth' => $date,
);

$children_existing = '';
if ($c = $d->listing('Child', 'Name', array('parent_id' => $parent))) {
    foreach ($c as $v) {
        if ($children_existing) $children_existing .= ', ';
        $children_existing .= $v;
    }
}

if (is_full($a)) {
    $child = db_save('Child', $a);
    return go('child.php?id=' . $parent);
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Tullamore Coder Dojo Management</title>
<link rel="icon" href="logo.png">
<style>
#logo{float:right}body{background-color:#009;color:#fff}input{display:block}
</style>
</head>
<body>
<div id="container" style="width:1000px">
<div id="header" style="background-color:#0000ff;">
<a href="http://coderdojotullamore.net"><img id="logo" src="tullamoreCD6.png" height="175" width="190" alt="Tullamore Coder Dojo Logo"></a>
<h1 style="margin-bottom:25px; color: #fff;">Tullamore Coder Dojo Signup</h1></div>
<p>Children of: <?=htmlentities($p['FirstName'] . ' ' . $p['LastName'])?></p>
<?php
if ($children_existing) {
    echo '<p>Children already entered: ' . htmlentities($children_existing) . '</p>' . PHP_EOL;
}
?>
<h2>Enter one child's details</h2>
<form method="post" action="">
<input type="text" maxlength="50" placeholder="name" name="Name">
<input type="number" placeholder="month (numeric: 1-12)" name="Month">
<input type="number" placeholder="year" name="Year">
<input type="submit" value="Save Data">
</form>
</div>
<div id="footer" style="background-color:#0000ff;clear:both;text-align:center;">&copy; CoderDojoTullamore</div>
</div>
</body>
</html>

