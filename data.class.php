<?php

class Data {

    public
        $id      = 'id';
    private
        $link,
        $server,
        $username,
        $password,
        $database;
    static
        $queries = 0,
        $sql,
        $sqls    = array();
        // We're not creating a global $db = new Data and using it everywhere. We're making it new each time.
        // That's why we need static variables to hold this data. Used for tracking purposes only. (Perhaps displayed if signed in as developer?)

    public function __construct($name='data') {
        $a = get_config($name);
        $this->server   = value($a, 'server',   $this->server,   true);
        $this->database = value($a, 'database', $this->database, true);
        $this->username = value($a, 'username', $this->username, true);
        $this->password = value($a, 'password', $this->password, true);
        $this->config   = $name; // for tracking purposes only
    }
    private function open() {
        $this->link = mysql_connect($this->server, $this->username, $this->password) or exit('OpenConn: ' . mysql_error());
        mysql_select_db($this->database, $this->link) or exit('OpenDatabase: "' . $this->database . '".');
        return true;
    }
    public function escape($str) {
        if (!$this->link) $this->open(); // Need to open here, or mysql_real_escape_string() returns false.
        return mysql_real_escape_string($str);
    }
    public function run($sql) {
        self::$queries++;
        self::$sqls[]  = array('config' => $this->config, 'sql' => $sql);
        self::$sql     = $sql;
        if (!$this->link) $this->open();
        if (($this->result = mysql_query(self::$sql, $this->link)) === false) return false;
        if (($this->affected = mysql_affected_rows($this->link)) === -1) return false;
        return true;
    }
    public function row($table, $where, $vals=0, $extra='') {
        $a = $this->select($table, $vals, $where, $extra);
        return $a[0];
    }
    public function value($table, $where, $val, $extra='', $default='', $return=1) {
        $a = $this->select($table, $val, $where, $extra);
        if (($return) and ($a[0][$val] === '0')) return 0;
        return ($a[0][$val] ? $a[0][$val] : $default);
    }
    public function listing($table, $val, $where='', $extra='') {
        $a = $this->select($table, array($this->id, $val), $where, $extra);
        $rows = array();
        foreach ($a as $v) {
            $rows[$v[$this->id]] = $v[$val];
        }
        return $rows;
    }
    public function select($table, $vals=0, $where='', $extra='') {
        $q = '';
        if (is_array($vals)) {
            foreach ($vals as $v) {
                if ($q) $q .= ', ';
                $q .= '`' . $this->escape($v) . '`';
            }
        } elseif (!$vals) {
            $q = '*';
        } else {
            $q = $vals;
        }
        if (is_array($where)) {
            $p = '';
            foreach ($where as $k => $v) {
                if ($p) $p .= ' AND';
                $p .= ' (`' . $this->escape($k) . '`="' . $this->escape($v) . '")';
            }
            $where = $p;
        } elseif (Check::isint($where)) {
            $where = '`' . $this->id . '`=' . $where;
        }
        $sql = 'SELECT ' . $q . ' FROM `' . $table . '`' . ($where ? ' WHERE ' . $where : '') . $extra;
        return $this->load($sql);
    }
    public function load($sql) { // For custom selects. Custom updates, inserts, and replaces can use this->run().
        if (!$this->run($sql)) return false;
        $this->rows = mysql_num_rows($this->result);
        $rows = array(); // so if no result returns empty array, not null. Thus it should be safe to use the result in a foreach loop.
        while ($row = mysql_fetch_assoc($this->result)) $rows[]= $row;
        return $rows;
    }
    public function insert($table, $arr) {
        $fields = '';
        $values = '';
        foreach ($arr as $k => $v) {
            if ($fields) $fields .= ', ';
            if ($values) $values .= ', ';
            $fields .= '`' . $this->escape($k) . '`';
            $values .= '"' . $this->escape($v) . '"';
        }
        $sql = 'INSERT INTO `' . $table . '` (' . $fields . ') VALUES (' . $values . ')';
        if ($this->run($sql)) return $this->insert_id = mysql_insert_id();
        return false;
    }
    public function update($table, $arr, $where='') {
        $u = '';
        foreach ($arr as $k => $v) {
            if ($u) $u .= ', ';
            $u .= '`' . $this->escape($k) . '`="' . $this->escape($v) . '"';
        }
        $sql = 'UPDATE `' . $table . '` SET ' . $u;
        if (is_array($where)) {
            $p = '';
            foreach ($where as $k => $v) {
                if ($p) $p .= ' AND';
                $p .= ' (`' . $this->escape($k) . '`="' . $this->escape($v) . '")';
            }
            $where = $p;
        } elseif (Check::isint($where)) {
            $where = '`' . $this->id . '`=' . $where;
        }
        $sql .= ($where ? ' WHERE ' . $where : '');
        return $this->run($sql);
    }
}

