<?php

class Check {
    public static function isint(&$n) {
        if ((string)$n === '0') return true;
        if ($n === true) return false;
        if (!$n) return false;
        $i = (int)$n;
        return ($i == $n);
    }
    public static function email(&$mail) {
        exit(__FILE__ . ': ' . __LINE__);
        return (filter_var($mail, FILTER_VALIDATE_EMAIL) ? true : false);
    }
    public static function web(&$url) {
        exit(__FILE__ . ': ' . __LINE__);
        return (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED) ? true : false);
    }
    /*
    public static function number(&$n) {
        return is_numeric($n);
    }
    */
    public static function tel(&$num, $min, $max, &$e) {
        exit(__FILE__ . ': ' . __LINE__);
        $n = 0;
        $l = mb_strlen($num);
        for ($i = 0; $i < $l; $i++) {
            $c = mb_substr($num, $i, 1);
            if (($c == '+') and ($i == 1)) {
            } elseif (($c == '(') or ($c == ')') or ($c == '-') or ($c == '.') or ($c == ' ') or ($c == '[') or ($c == ']')) {
            } elseif (ctype_digit($c)) {
                $n++;
            } else {
                $e = 'Non-numeric';
                return false;
            }
            if ($n < $min) {
                $e = 'min';
                return false;
            }
            if ($n > $max) {
                $e = 'max';
                return false;
            }
            return true;
        }
    }
}

