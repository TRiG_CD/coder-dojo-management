<?php


ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);
spl_autoload_register('load_class');


function value($arr, $name, $default) {
    return isset($arr[$name]) ? $arr[$name] : $default;
}
function post($name, $default='') {
    return value($_POST, $name, $default);
}
function get($name, $default='') {
    return value($_GET, $name, $default);
}
function get_config($name) {
    static $config = array();
    if (array_key_exists($name, $config)) return $config[$name];
    return $config[$name] = require $name . '.config.php';

}
function is_full($arr) {
    foreach ($arr as $v) {
        if (!$v) return false;
    }
    return true;
}
function db_save($table, $arr) {
    $d = new Data;
    return $d->insert($table, $arr);
}
function load_class($classname) {
    require strtolower($classname) . '.class.php';
}
function go($url) {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $url);
    header('Content-Type: text/plain');
    echo 'Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $url . PHP_EOL;
    exit;
}

