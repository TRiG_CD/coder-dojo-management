<?php
require 'functions.php';

$email = post('Email');
$email = filter_var($email, FILTER_VALIDATE_EMAIL);

$a = array(
    'FirstName'  => post('FirstName'),
    'LastName'   => post('LastName'),
    'Telephone'  => post('Telephone'),
    'Email'      => $email,
);

if (is_full($a)) {
    $parent = db_save('Parent', $a);
    return go('child.php?id=' . $parent);
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Tullamore Coder Dojo Management</title>
<link rel="icon" href="logo.png">
<style>
#logo{float:right}body{background-color:#009}input{display:block}
</style>
</head>
<body>
<div id="container" style="width:1000px">
<div id="header" style="background-color:#0000ff;">
<a href="http://coderdojotullamore.net"><img id="logo" src="tullamoreCD6.png" height="175" width="190" alt="Tullamore Coder Dojo Logo"></a>
<h1 style="margin-bottom:25px; color: #fff;">Tullamore Coder Dojo Signup</h1></div>
<div id="content" style="background-color:#000099;height:500px;width:750px;float:left;">
<form method="post" action="/">
<input type="text" maxlength="50" placeholder="first name" name="FirstName">
<input type="text" maxlength="50" placeholder="last name" name="LastName">
<input type="text" maxlength="50" placeholder="telephone" name="Telephone">
<input type="email" maxlength="50" placeholder="e-mail" name="Email">
<input type="submit" value="Save Data">
</form>
</div>
<div id="footer" style="background-color:#0000ff;clear:both;text-align:center;">&copy; CoderDojoTullamore</div>
</div>
</body>
</html>

